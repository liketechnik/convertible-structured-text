<!--
SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>

SPDX-License-Identifier: MPL-2.0
-->

# Inline

- Highlighting
	- Italic => '**'
	- Bold => '*'
	- Strikethrough => '~'
	- Underlined => '_'
- Command(s) => '/command_name(param1=someval) { content /}'
	Available as both inline or standalone command.
	Currently implemented:
	- include (only standalone): Include the content of another file. (the filename is the command's content)
	- image (inline/standalone): Insert an image (the filename is the command's content)
- Comments => '//' -> linend
- Math => '$'
- Code (unspecific) => '`', '`'

# Sections

- Title => Start of line -> '#' -> Text (mit Inlinefunktionalität) -> End of line
- Paragraph => Zeilenstart -> Text (mit Inlinefunktionalität) -> Leerzeile | Dateiende
- Comment => Zeilenstart -> '//' -> Text (ohne Inlinefunktionalität) -> Zeilenende
- Command(s) => Zeilenstart -> ? -> Command Name -> '(' -> Parameter -> ')' -> ? -> '{' -> Inhalt -> '}' -> Zeilenende
	- Code (optional language specific) 
	- Math
	- Image
	- Lists (unordered, ordered, definition list) [parse with seperate grammar]
