<!--
SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>

SPDX-License-Identifier: MPL-2.0
-->

# Convertible structured text

Generate docx files from a mix of markdown, reStructuredText and LaTex syntax.

## Index

* [About](#about)
* [Example](#example)
* [Current state](#current-state)
	* [Pros](#pros)
	* [Cons / Problems](#cons-problems)
	* [Roadmap](#roadmap)
* [Details / Inner workings](#details--inner-workings)
* [Dependencies](#dependencies)
	* [Nom](#nom)
	* [docx-rs](#docx-rs)
	* [thiserror / anyhow](#thiserror-anyhow)
* [License](#license)

## About

For me, working with WYSIWYG editors like Word/LibreOffice usually is a huge pain.
But since often institutions require documents to be in .doxc format,
there's not really a way around Word 
(although there are conversion tools, the resulting Word document usually has some [minor] issues).

Because I'm a huge rust fan (and don't like dynamically typed or interpreted languages) I decided
to try and write my own 'converter' for this task.

Currently it can't do much, but I have a lot of features I want to add, see the [Roadmap](#roadmap)
or the issue list.

## Example

See [examples/](examples/) for some example projects. 
I plan to add more examples, though currently [examples/simple](examples/simple) 
is the only demo.
You can convert the demo project's source files by 
* building all workspace crates: `cargo build --all`
* copying the render plugin from `target/debug/libcst_render_docx.so` to `examples/simple/plugins`
* executing cst with `cargo run -- -p examples/simple`
after cloning it.

## Current state

Generally this program has pre-alpha quality at the moment. 
Most things only have a rudimentary 
(or sometimes incorrect, only showcasing functionality, like code highlighting)
implementation. But there's a lot to come.

### Pros

- .docx-format is a first class target for conversion
- Easily version control your documents, cause their only plaintext
- extensible (in the future):
	- support for multiple output formats
	- support for different input formats
- Due to the program being written in rust, it's possible to statically compile it,
	which makes it possible to use the program in situations where a converter
	written in interpreted languages would not be usable

### Cons / Problems

- other tools already support parsing and other output formats,
	extending one of these tools could provide more value
- most things don't work yet

### Roadmap

- [ ] Configuration system, CLI interface, project templates
- [ ] Plugin system (supporting more input and output formats)
- [ ] More/better testing
- [ ] Support of additional markup structures (literal text, colors, font (size), lists, etc.)
- [ ] Actually adding support for more output/input formats

## Details / Inner workings

A plaintext file with markdown like markup is parsed into an AST.
This AST is then handed to a renderer that produces the output file.
See [specification.md](specification.md) for the markup syntax used.

## Dependencies

The following libraries enable the functionality of this program:

### Nom

[Nom](https://github.com/Geal/nom) is, after some experimentation with different parser/lexer
libraries in rust, my definitive favorite when it comes to parsing. It enables the parsing of
plain text into an AST (and has a really wonderful API).

### docx-rs

[docx-rs](https://github.com/bokuweb/docx-rs) is a .doxc file writer written in/for rust.
Without its existance this project couldn't server its use.

### thiserror / anyhow

[thiserror](https://github.com/dtolnay/thiserror) / [anyhow](https://github.com/dtolnay/anyhow)
hopefully provides you with helpfull error messages in case something brakes.

## License

[![REUSE status](https://api.reuse.software/badge/gitlab.com/liketechnik/convertible-structured-text)](https://api.reuse.software/info/gitlab.com/liketechnik/convertible-structured-text)

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
