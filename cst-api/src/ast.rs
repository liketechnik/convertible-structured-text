// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

#[derive(Debug, PartialEq, Clone)]
pub enum Document {
    Paragraph(Vec<TextComponent>),
    Comment(String),
    Command(Command),
    Title {
        level: usize,
        text: Vec<TextComponent>,
    },
}

#[derive(Debug, PartialEq, Clone)]
pub enum TextComponent {
    Whitespace(String),
    Plain(String),
    Italic(Vec<TextComponent>),
    Bold(Vec<TextComponent>),
    Strikethrough(Vec<TextComponent>),
    Underlined(Vec<TextComponent>),
    Code(String),
    Math(String),
    Command(Command),
}

#[derive(Debug, PartialEq, Clone)]
pub struct Command {
    pub name: String,
    pub parameters: Vec<Parameter>,
    pub content: CommandContent,
}

#[derive(Debug, PartialEq, Clone)]
pub enum CommandContent {
    Raw(String),
    Document(Vec<Document>),
    TextComponent(Vec<Document>),
}

#[derive(Debug, PartialEq, Clone)]
pub struct Parameter {
    pub name: String,
    pub value: String,
}
