// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

//! Allow the loading and creation of plugin libraries.
//!
//! Plugins can be created with one of the
//! `[export_render!()]` macros.
//!
//! For a list of supported plugin types see `[Plugin]`.
//!
//! Note that only one type per library/crate is supported,
//! multiple `export_*!()` calls will result in a compile error.
//!
//! The original idea for this plugin system in rust
//! comes from the following blog entry: http://adventures.michaelfbryan.com/posts/plugins-in-rust/
//! The code for the plugin system laid out there is at
//! https://github.com/Michael-F-Bryan/plugins_in_rust, which is licensed under MIT/APACHE.

use crate::ast::Command;
use crate::ast::TextComponent;
use anyhow::Result;
use std::path::Path;

use crate::ast::Document;
use crate::config::Configuration;

/// A render plugin, which renders the AST into a document.
pub struct RenderPlugin {
    /// The function to render the AST.
    pub render_fn: RenderFn,
    /// Commands supported by the render.
    pub commands: &'static [&'static str],
    /// The file type this plugin produces.
    pub file_ending: &'static str,
}

pub struct CommandPlugin {
    pub commands: &'static [(&'static str, CommandFn)],
    pub inline_commands: &'static [(&'static str, InlineCommandFn)],
}

pub struct ParserPlugin {
    pub parse_fn: ParseFn,
    pub file_ending: &'static str,
}

/// A plugin exported with `[PluginDeclaration]`.
///
/// Use one of the macros to actually create a plugin.
pub enum PluginDeclaration {
    Render(RenderPlugin),
    Command(CommandPlugin),
    Parser(ParserPlugin),
}

/// Function signature for a render plugin.
///
/// Responsible for transforming the `[Document]` AST into
/// a document at `[out_file]`.
pub type RenderFn = fn(nodes: &[Document], out_file: &Path, config: &Configuration) -> Result<()>;

pub type CommandFn = fn(&mut Command, &Configuration) -> Result<Vec<Document>>;
pub type InlineCommandFn = fn(&mut Command, &Configuration) -> Result<Vec<TextComponent>>;

pub type ParseFn = fn(&str, &Configuration) -> Result<Vec<Document>>;

/// Automatically create the declaration of a render plugin.
///
/// Creates a static variable containing the `[PluginDeclaration]` for
/// the render plugin.
///
/// First specify the file type for the document the plugin produces,
/// then the name of the function that transforms the ast,
/// and the commands that are supported by the plugin.
#[macro_export]
macro_rules! export_render {
    ($file_ending:expr, $render_func:expr, $commands:expr) => {
        #[doc(hidden)]
        #[no_mangle]
        pub const plugin_declaration: $crate::plugin::PluginDeclaration =
            $crate::plugin::PluginDeclaration::Render($crate::plugin::RenderPlugin {
                file_ending: $file_ending,
                render_fn: $render_func,
                commands: $commands,
            });
    };
}

/// Automatically create the declaration of a command plugin.
///
/// Creates a static variable containing the `[PluginDeclaration]` for
/// the command plugin.
///
/// First specify an array of `name` to `command` tuples/mappings for
/// 'document level' commands, then an array of `name` to `command`
/// tuples mappings for inline commands.
#[macro_export]
macro_rules! export_commands {
    ($commands:expr, $inline_commands:expr) => {
        #[doc(hidden)]
        #[no_mangle]
        pub const plugin_declaration: $crate::plugin::PluginDeclaration =
            $crate::plugin::PluginDeclaration::Command($crate::plugin::CommandPlugin {
                commands: $commands,
                inline_commands: $inline_commands,
            });
    };
}

/// Automatically create the declaration of a parser plugin.
///
/// Creates a static variable containing the `[PluginDeclaration]` for
/// the parser plugin.
///
/// First specify the file ending this plugin consumes, then the name
/// of the function that parses a `&str` into an AST.
#[macro_export]
macro_rules! export_parser {
    ($file_ending:expr, $parse_fn:expr) => {
        #[doc(hidden)]
        #[no_mangle]
        pub const plugin_declaration: $crate::plugin::PluginDeclaration =
            $crate::plugin::PluginDeclaration::Parser($crate::plugin::ParserPlugin {
                file_ending: $file_ending,
                parse_fn: $parse_fn,
            });
    };
}
