// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

//! Command line options.
//!
//! This module contains the parsing of given command line options.

use clap::Clap;

use std::path::PathBuf;

#[derive(Clap, Debug)]
#[clap(
    name = "Convertible Structured Text",
    author,
    version,
    about,
    after_help = r"Copyright (C) 2020 

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/."
)]
pub struct Options {
    /// The root directory of the project to work with.
    #[clap(short, long, default_value = ".", validator=validate_dir_exists)]
    pub(crate) project: String,
}

fn validate_dir_exists(v: &str) -> Result<(), String> {
    if PathBuf::from(v).is_dir() {
        Ok(())
    } else {
        Err(String::from("The directory does not exist"))
    }
}
