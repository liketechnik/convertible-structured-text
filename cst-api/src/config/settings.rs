// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

//! Workspace configuration.
//!
//! This module contains type to parse the workspace
//! configuration for a cst project.

use anyhow::{anyhow, Result};
use config::{Config, File, FileFormat};
use serde::{Deserialize, Serialize};

use std::path::PathBuf;

use crate::config::Options;

#[derive(Debug, Serialize, Deserialize)]
pub struct Settings {
    pub(crate) project: Project,
    #[serde(default)]
    pub(crate) build: Build,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Build {
    #[serde(default)]
    pub(crate) in_file: Option<String>,
    #[serde(default)]
    pub(crate) out_file: Option<String>,
    #[serde(default = "default_in_dir")]
    pub(crate) in_dir: String,
    #[serde(default = "default_out_dir")]
    pub(crate) out_dir: String,
}
fn default_in_dir() -> String {
    ".".to_string()
}
fn default_out_dir() -> String {
    ".".to_string()
}
impl Default for Build {
    fn default() -> Self {
        Build {
            in_file: None,
            out_file: None,
            in_dir: default_in_dir(),
            out_dir: default_out_dir(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Project {
    pub(crate) name: String,
}

impl Settings {
    pub fn new(opts: &Options) -> Result<Self> {
        let mut config = Config::new();

        let mut path = PathBuf::from(opts.project.clone());
        path.push("cst.toml");

        config.merge(File::new(
            path.to_str()
                .ok_or_else(|| anyhow!("Path is invalid UTF-8"))?,
            FileFormat::Toml,
        ))?;

        Ok(config.try_into()?)
    }
}
