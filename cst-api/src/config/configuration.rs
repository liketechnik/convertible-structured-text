// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

//! Runtime configuration.
//!
//! This module contains the types that are used to handle
//! all configuration at runtime, e. g. after the initial settings
//! have been loaded (see `super::options` and `super::settings`).

use crate::plugin::CommandFn;
use crate::plugin::InlineCommandFn;
use crate::plugin::ParseFn;
use crate::plugin::RenderPlugin;

use std::collections::HashMap;
use std::path::Path;
use std::path::PathBuf;

use crate::config::Build;
use crate::config::Options;
use crate::config::Project;
use crate::config::Settings;

pub type RenderPlugins = HashMap<&'static str, &'static RenderPlugin>;
pub type ParserPlugins = HashMap<&'static str, ParseFn>;

pub struct Configuration {
    pub commands: CommandRegistry,
    pub project: ProjectConfig,
    pub workdir: PathBuf,
    pub build: BuildConfig,
    pub parsers: ParserPlugins,
    pub renders: RenderPlugins,
}

pub struct CommandRegistry {
    pub parser_commands: HashMap<&'static str, CommandFn>,
    pub parser_inline_commands: HashMap<&'static str, InlineCommandFn>,
    pub render_commands: Vec<&'static str>,
}

pub struct ProjectConfig {
    pub name: String,
}

impl From<Project> for ProjectConfig {
    fn from(project: Project) -> Self {
        ProjectConfig { name: project.name }
    }
}

pub struct BuildConfig {
    pub in_dir: PathBuf,
    pub in_file: PathBuf,
    pub out_dir: PathBuf,
    pub out_file: PathBuf,
}

impl BuildConfig {
    /// Compose the runtime config from
    /// the various options and settings.
    fn from(build: Build, project: &Project, workdir: &Path) -> BuildConfig {
        let mut in_dir = workdir.to_path_buf();
        in_dir.push(build.in_dir);

        let mut in_file = in_dir.clone();
        if let Some(name) = build.in_file {
            in_file.push(name);
        } else {
            in_file.push(&project.name);
        }

        let mut out_dir = workdir.to_path_buf();
        out_dir.push(build.out_dir);

        let mut out_file = out_dir.clone();
        if let Some(name) = build.out_file {
            out_file.push(name);
        } else {
            out_file.push(&project.name);
        }

        BuildConfig {
            in_dir,
            in_file,
            out_dir,
            out_file,
        }
    }
}

impl Configuration {
    pub fn new(
        settings: Settings,
        options: Options,
        commands: CommandRegistry,
        renders: RenderPlugins,
        parsers: ParserPlugins,
    ) -> Self {
        let workdir = PathBuf::from(options.project);

        let build = BuildConfig::from(settings.build, &settings.project, &workdir);

        let project = settings.project.into();

        Configuration {
            commands,
            project,
            workdir,
            build,
            renders,
            parsers,
        }
    }
}

/// Traits that restrict the mutability
/// of a mutable `Configuration`,
/// so that the receiver of such a trait object
/// still has full access to all properties,
/// but can only access a certain set of them.
///
/// This allows to always pass the whole `Configuration`
/// struct, while being sure that only certain properties can be changed.
pub mod view {}
