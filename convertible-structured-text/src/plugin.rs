// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use anyhow::{anyhow, Result};
use cst_api::config::{ParserPlugins, RenderPlugins};
use cst_api::plugin::CommandFn;
use cst_api::plugin::CommandPlugin;
use cst_api::plugin::InlineCommandFn;
use cst_api::plugin::ParserPlugin;
use cst_api::plugin::PluginDeclaration;
use cst_api::plugin::RenderPlugin;
use std::collections::HashMap;

const PLUGINS: &[&PluginDeclaration] = &[
    &cst_render_docx::plugin_declaration,
    &cst_parser_cst::plugin_declaration,
    &cst_commands_basic::plugin_declaration,
];

type Commands = HashMap<&'static str, CommandFn>;
type InlineCommands = HashMap<&'static str, InlineCommandFn>;

pub fn load_plugins() -> Result<(
    RenderPlugins,
    Vec<&'static str>,
    Commands,
    InlineCommands,
    ParserPlugins,
)> {
    let mut render_plugins: RenderPlugins = HashMap::new();
    let mut render_commands: Vec<&'static str> = Vec::new();
    let mut commands = HashMap::new();
    let mut inline_commands = HashMap::new();
    let mut parser_plugins = HashMap::new();

    for plugin in PLUGINS {
        match plugin {
            PluginDeclaration::Render(plugin) => {
                load_render(&mut render_plugins, &mut render_commands, plugin)?
            }
            PluginDeclaration::Command(plugin) => {
                load_commands(&mut commands, &mut inline_commands, plugin)?
            }
            PluginDeclaration::Parser(plugin) => load_parser(&mut parser_plugins, plugin)?,
        }
    }

    Ok((
        render_plugins,
        render_commands,
        commands,
        inline_commands,
        parser_plugins,
    ))
}

fn load_parser(plugins: &mut ParserPlugins, plugin: &'static ParserPlugin) -> Result<()> {
    if plugins.contains_key(plugin.file_ending) {
        return Err(anyhow!(
            "Tried to register multiple parsers for the same filetype!"
        ));
    }

    plugins.insert(plugin.file_ending, plugin.parse_fn);

    Ok(())
}

fn load_commands(
    commands: &mut Commands,
    inline_commands: &mut InlineCommands,
    plugin: &'static CommandPlugin,
) -> Result<()> {
    for (name, command) in plugin.commands {
        if commands.contains_key(name) {
            return Err(anyhow!(
                "Tried to register multiple commands with the same name!"
            ));
        }

        commands.insert(name, *command);
    }

    for (name, inline_command) in plugin.inline_commands {
        if inline_commands.contains_key(name) {
            return Err(anyhow!(
                "Tried to register multiple inline commands with the same name!"
            ));
        }

        inline_commands.insert(name, *inline_command);
    }

    Ok(())
}

fn load_render(
    plugins: &mut RenderPlugins,
    commands: &mut Vec<&'static str>,
    plugin: &'static RenderPlugin,
) -> Result<()> {
    if plugins.contains_key(plugin.file_ending) {
        return Err(anyhow!(
            "Tried to register multiple render plugins for the same file type!"
        ));
    }

    for command in plugin.commands {
        commands.push(command);
    }

    plugins.insert(plugin.file_ending, plugin);

    Ok(())
}
