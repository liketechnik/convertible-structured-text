// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use anyhow::{anyhow, Context, Result};
use clap::Clap;
use cst_api::config::CommandRegistry;
use cst_api::config::{Configuration, Options, Settings};

use std::ffi::OsString;
use std::fs;

mod plugin;

pub fn main<I, T>(arguments: I) -> Result<()>
where
    I: IntoIterator<Item = T>,
    T: Into<OsString> + Clone,
{
    let opts = Options::parse_from(arguments);
    let settings = Settings::new(&opts)?;

    let (renders, render_commands, parser_commands, parser_inline_commands, parsers) =
        plugin::load_plugins()?;
    let commands = CommandRegistry {
        parser_commands,
        parser_inline_commands,
        render_commands,
    };

    let config = Configuration::new(settings, opts, commands, renders, parsers);

    if let Some(extension) = config.build.in_file.extension() {
        let parser = match extension.to_string_lossy() {
            std::borrow::Cow::Owned(ext) => config.parsers.get(ext.as_str()),
            std::borrow::Cow::Borrowed(ext) => config.parsers.get(ext),
        };
        if let Some(parser) = parser {
            let unparsed_file = fs::read_to_string(&config.build.in_file).with_context(|| {
                format!("Could not open {}", config.build.in_file.to_string_lossy())
            })?;

            let content = parser(&unparsed_file, &config)?;

            for (file_ending, render) in config.renders.iter() {
                let mut out_file = config.build.out_file.clone();
                out_file.set_extension(file_ending);
                (render.render_fn)(&content, &out_file, &config)?;
            }

            return Ok(());
        }
    }

    Err(anyhow!("No parser for input file!"))
}
