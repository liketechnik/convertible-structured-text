// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use anyhow::Result;

fn main() -> Result<()> {
    convertible_structured_text::main(std::env::args_os())
}
