// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use convertible_structured_text;

#[test]
fn simple_example() {
    let args = vec!["convertible-structured-text", "-p", "../examples/simple"];
    convertible_structured_text::main(args.iter()).unwrap();
}
