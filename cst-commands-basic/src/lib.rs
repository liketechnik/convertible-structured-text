// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use anyhow::{anyhow, Context, Result};
use cst_api::ast::Command;
use cst_api::ast::CommandContent;
use cst_api::ast::Document;
use cst_api::config::Configuration;

fn include(command: &mut Command, config: &Configuration) -> Result<Vec<Document>> {
    let command_content = match &command.content {
        CommandContent::Raw(content) => content,
        _ => {
            return Err(anyhow!("Expected raw command content!"));
        }
    };

    let mut in_file = config.build.in_dir.clone();
    in_file.push(command_content);

    if let Some(extension) = in_file.extension() {
        let parser = match extension.to_string_lossy() {
            std::borrow::Cow::Owned(ext) => config.parsers.get(ext.as_str()),
            std::borrow::Cow::Borrowed(ext) => config.parsers.get(ext),
        };
        if let Some(parser) = parser {
            let unparsed_file = std::fs::read_to_string(&in_file)
                .with_context(|| format!("Could not open {}", in_file.to_string_lossy()))?;

            let content = parser(&unparsed_file, &config)?;

            for (file_ending, render) in config.renders.iter() {
                let mut out_file = config.build.out_file.clone();
                out_file.set_extension(file_ending);
                (render.render_fn)(&content, &out_file, &config)?;
            }

            return Ok(content);
        }
    }

    Err(anyhow!("No parser for input file!"))
}

cst_api::export_commands!(&[("include", include)], &[]);
