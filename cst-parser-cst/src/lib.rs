// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use anyhow::anyhow;
use cst_api::ast::{Command, CommandContent, Document, Parameter, TextComponent};
use cst_api::config::Configuration;
use nom::combinator::opt;
use nom::{
    branch::alt,
    bytes::complete::{tag, take_until},
    character::complete::{self, line_ending, none_of, one_of, space0, space1},
    combinator::{cut, map, rest_len},
    error::context as ctx,
    error::make_error,
    multi::{fold_many0, fold_many1, many0, many1, many1_count, separated_list},
    sequence::{delimited, pair, preceded, separated_pair},
    Err::Failure,
    IResult,
};

use std::cell::RefCell;

pub mod error;
#[cfg(test)]
mod test;

use error::VerboseError;

const BOLD: char = '*';
const ITALIC: &str = "**";
const STRIKETHROUGH: char = '~';
const UNDERLINED: char = '_';
const MATH: char = '$';
const CODE: char = '`';
const COMMAND: char = '/';
const PARAM_START: char = '(';
const PARAM_END: char = ')';
const PARAM_ASSIGN: char = '=';
const PARAM_SEP: char = ',';
const COMMAND_START: char = '{';
const COMMAND_END: &str = "/}";
const COMMENT: &str = "//";

pub type ParseResult<'i, C> = IResult<&'i str, C, VerboseError<'i>>;

enum DocumentParseResult {
    None,
    Some(Document),
    Many(Vec<Document>),
}

cst_api::export_parser!("cst", parse_str);

pub fn parse_str(content: &str, config: &Configuration) -> anyhow::Result<Vec<Document>> {
    Ok(parse(&config)(&content)
        .map_err(|e| {
            let e = match e {
                nom::Err::Error(e) => e,
                nom::Err::Failure(e) => e,
                nom::Err::Incomplete(_) => panic!("Unexpected variant!"),
            };
            let error = error::convert_error(&content, e);
            anyhow!("{}", error)
        })?
        .1)
}

pub fn parse<'i: 'c, 'c>(
    config: &'c Configuration,
) -> impl Fn(&'i str) -> ParseResult<'i, Vec<Document>> + 'c {
    move |input: &str| {
        fold_many0(
            cond(
                eoi,
                cut(alt((
                    map(document_command(config), DocumentParseResult::Many),
                    map(comment, DocumentParseResult::Some),
                    map(title(config), DocumentParseResult::Some),
                    map(paragraph(config), DocumentParseResult::Some),
                    map(line_ending, |_| DocumentParseResult::None),
                    map(space1, |_| DocumentParseResult::None),
                ))),
                true,
            ),
            Vec::new(),
            |mut acc: Vec<_>, item| {
                match item {
                    DocumentParseResult::None => (),
                    DocumentParseResult::Some(doc) => acc.push(doc),
                    DocumentParseResult::Many(mut docs) => acc.append(&mut docs),
                }
                acc
            },
        )(input)
    }
}

fn cond<'i, C, P>(
    condition: C,
    parser: P,
    invert: bool,
) -> impl Fn(&'i str) -> ParseResult<'i, DocumentParseResult>
where
    C: Fn(&'i str) -> ParseResult<'i, bool>,
    P: Fn(&'i str) -> ParseResult<'i, DocumentParseResult>,
{
    move |input: &str| {
        let mut condition = condition(input)?.1;
        if invert {
            condition = !condition;
        }

        if condition {
            parser(input)
        } else {
            Err(nom::Err::Error(nom::error::make_error(
                input,
                nom::error::ErrorKind::Eof,
            )))
        }
    }
}

fn eoi(input: &str) -> ParseResult<bool> {
    Ok((input, rest_len(input)?.1 == 0))
}

fn comment(input: &str) -> ParseResult<Document> {
    let (remainder, comment) = delimited(
        tag(COMMENT),
        // we can use opt here, because the only way `context_str` can fail
        // here is for an 'emtpy comment' (e. g. only the comment markers)
        opt(context_str(&MarkupContext::Comment)),
        line_ending,
    )(input)?;
    Ok((
        remainder,
        Document::Comment(comment.unwrap_or_else(|| "".to_string())),
    ))
}

fn document_command<'i: 'c, 'c>(
    config: &'c Configuration,
) -> impl Fn(&'i str) -> ParseResult<'i, Vec<Document>> + 'c {
    move |input: &str| {
        let (remainder, mut command) = command(input)?;

        let command_result = match config.commands.parser_commands.get(&command.name.as_str()) {
            Some(command_execute) => command_execute(&mut command, &config)
                .map_err(|e| VerboseError::from_nested(remainder, e))
                .map_err(nom::Err::Failure),
            None => {
                if config
                    .commands
                    .render_commands
                    .contains(&command.name.as_str())
                {
                    return Ok((remainder, vec![Document::Command(command)]));
                } else {
                    return Err(Failure(VerboseError::from_error(
                        remainder,
                        anyhow!("Unknown command: {}", command.name),
                    )));
                }
            }
        }?;

        Ok((remainder, command_result))
    }
}

fn inline_command<'i: 'c, 'c>(
    config: &'c Configuration,
) -> impl Fn(&'i str) -> ParseResult<'i, Vec<TextComponent>> + 'c {
    move |input: &str| {
        let (remainder, mut command) = command(input)?;

        let command_result = match config
            .commands
            .parser_inline_commands
            .get(&command.name.as_str())
        {
            Some(command_execute) => command_execute(&mut command, &config)
                .map_err(|e| VerboseError::from_nested(remainder, e))
                .map_err(nom::Err::Failure),
            None => {
                if config
                    .commands
                    .render_commands
                    .contains(&command.name.as_str())
                {
                    return Ok((remainder, vec![TextComponent::Command(command)]));
                } else {
                    return Err(Failure(VerboseError::from_error(
                        remainder,
                        anyhow!("Unknown command: {}", command.name),
                    )));
                }
            }
        }?;

        Ok((remainder, command_result))
    }
}

fn command(input: &str) -> ParseResult<Command> {
    let (remainder, name) = delimited(
        complete::char(COMMAND),
        context_str(&MarkupContext::CommandName),
        complete::char(PARAM_START),
    )(input)?;
    let (remainder, parameters) =
        separated_list(pair(complete::char(PARAM_SEP), space0), parameter)(remainder)?;

    let (remainder, _) = complete::char(PARAM_END)(remainder)?;
    let (remainder, _) = many0(alt((line_ending, space1)))(remainder)?;
    let (remainder, _) = complete::char(COMMAND_START)(remainder)?;

    let (remainder, content) = take_until(COMMAND_END)(remainder)?;

    let (remainder, _) = tag(COMMAND_END)(remainder)?;

    Ok((
        remainder,
        Command {
            name,
            parameters,
            content: CommandContent::Raw(content.to_string()),
        },
    ))
}

fn parameter(input: &str) -> ParseResult<Parameter> {
    let (remainder, (name, value)) = separated_pair(
        context_str(&MarkupContext::Parameter),
        complete::char(PARAM_ASSIGN),
        context_str(&MarkupContext::Parameter),
    )(input)?;
    Ok((remainder, Parameter { name, value }))
}

fn paragraph<'i: 'c, 'c>(
    config: &'c Configuration,
) -> impl Fn(&'i str) -> ParseResult<'i, Document> + 'c {
    move |input: &str| {
        let mut content = vec![];

        let mut outer_remainder: &str = input;
        loop {
            let (mut _remainder, mut line) = text_line(config)(outer_remainder)?;
            outer_remainder = _remainder;
            content.append(&mut line);

            // exit conditions
            // are we at the end of input?
            if eoi(outer_remainder)?.1 {
                break;
            }

            // is there another empty newline?
            let (remainder, _) = many0(text_whitespace)(outer_remainder)?;
            let has_newline: ParseResult<&str> = line_ending(remainder);
            if let Ok((remainder, _)) = has_newline {
                outer_remainder = remainder;
                break;
            }

            content.push(TextComponent::Whitespace("".to_string()));
        }

        Ok((outer_remainder, Document::Paragraph(content)))
    }
}

fn title<'i: 'c, 'c>(config: &'c Configuration) -> impl Fn(&'i str) -> ParseResult<Document> + 'c {
    move |input: &str| {
        let (remainder, level) = title_level(input)?;
        let (remainder, text) = text_line(config)(remainder)?;
        Ok((remainder, Document::Title { level, text }))
    }
}

fn text_whitespace(input: &str) -> ParseResult<TextComponent> {
    let (remainder, whitespace) = space1(input)?;
    Ok((remainder, TextComponent::Whitespace(whitespace.to_string())))
}

fn title_level(input: &str) -> ParseResult<usize> {
    ctx("title level", many1_count(complete::char('#')))(input)
}

enum TextLineResult {
    Some(TextComponent),
    Many(Vec<TextComponent>),
}

fn text_line<'i: 'c, 'c>(
    config: &'c Configuration,
) -> impl Fn(&'i str) -> ParseResult<'i, Vec<TextComponent>> + 'c {
    move |input: &str| {
        let (remainder, _) = space0(input)?;

        let context = RefCell::from(TextComponentContext::new());

        let (mut remainder, line) = fold_many1(
            alt((
                map(inline_command(config), TextLineResult::Many),
                map(text_component(&context), TextLineResult::Some),
            )),
            Vec::new(),
            |mut acc: Vec<_>, item| {
                match item {
                    TextLineResult::Some(text) => acc.push(text),
                    TextLineResult::Many(mut texts) => acc.append(&mut texts),
                }
                acc
            },
        )(remainder)?;
        if !eoi(remainder)?.1 {
            remainder = line_ending(remainder)?.0;
        }
        Ok((remainder, line))
    }
}

fn text_component<'i: 'c, 'c>(
    context: &'c RefCell<TextComponentContext>,
) -> impl Fn(&'i str) -> ParseResult<'i, TextComponent> + 'c {
    move |input: &str| {
        alt((
            text_italic(context),
            text_bold(context),
            text_strikethrough(context),
            text_underlined(context),
            text_math,
            text_code,
            text_plain,
            text_whitespace,
        ))(input)
    }
}

fn text_plain(input: &str) -> ParseResult<TextComponent> {
    let (remainder, plain) = ctx("plain", context_str(&MarkupContext::Plain))(input)?;
    Ok((remainder, TextComponent::Plain(plain)))
}

fn text_bold<'i: 'c, 'c>(
    context: &'c RefCell<TextComponentContext>,
) -> impl Fn(&'i str) -> ParseResult<TextComponent> + 'c {
    move |input: &str| {
        if context.borrow().bold {
            return Err(nom::Err::Error(make_error(
                input,
                nom::error::ErrorKind::Verify,
            )));
        }

        if context.borrow().italic {
            let result: ParseResult<&str> = tag(ITALIC)(input);
            if result.is_ok() {
                return Err(nom::Err::Error(make_error(
                    input,
                    nom::error::ErrorKind::Verify,
                )));
            }
        }

        let (remainder, bold) = ctx(
            "bold",
            delimited(
                map(complete::char(BOLD), |result| {
                    context.borrow_mut().bold = true;
                    result
                }),
                cut(many1(text_component(context))),
                cut(complete::char(BOLD)),
            ),
        )(input)?;

        context.borrow_mut().bold = false;
        Ok((remainder, TextComponent::Bold(bold)))
    }
}

fn text_italic<'i: 'c, 'c>(
    context: &'c RefCell<TextComponentContext>,
) -> impl Fn(&'i str) -> ParseResult<TextComponent> + 'c {
    move |input: &str| {
        if context.borrow().italic {
            return Err(nom::Err::Error(make_error(
                input,
                nom::error::ErrorKind::Verify,
            )));
        }

        let (remainder, italic) = ctx(
            "italic",
            delimited(
                map(tag(ITALIC), |result| {
                    context.borrow_mut().italic = true;
                    result
                }),
                cut(many1(text_component(context))),
                cut(tag(ITALIC)),
            ),
        )(input)?;

        context.borrow_mut().italic = false;
        Ok((remainder, TextComponent::Italic(italic)))
    }
}

fn text_strikethrough<'i: 'c, 'c>(
    context: &'c RefCell<TextComponentContext>,
) -> impl Fn(&'i str) -> ParseResult<TextComponent> + 'c {
    move |input: &str| {
        if context.borrow().strikethrough {
            return Err(nom::Err::Error(make_error(
                input,
                nom::error::ErrorKind::Verify,
            )));
        }

        let (remainder, strikethrough) = ctx(
            "strikethrough",
            delimited(
                ctx(
                    "strikethrough start",
                    map(complete::char(STRIKETHROUGH), |result| {
                        context.borrow_mut().strikethrough = true;
                        result
                    }),
                ),
                ctx("strikethrough content", many1(text_component(context))),
                ctx("strikethrough end", cut(complete::char(STRIKETHROUGH))),
            ),
        )(input)?;

        context.borrow_mut().strikethrough = false;
        Ok((remainder, TextComponent::Strikethrough(strikethrough)))
    }
}

fn text_underlined<'i: 'c, 'c>(
    context: &'c RefCell<TextComponentContext>,
) -> impl Fn(&'i str) -> ParseResult<TextComponent> + 'c {
    move |input: &str| {
        if context.borrow().underlined {
            return Err(nom::Err::Error(make_error(
                input,
                nom::error::ErrorKind::Verify,
            )));
        }

        let (remainder, underlined) = ctx(
            "underlined",
            delimited(
                ctx(
                    "underlined start",
                    map(complete::char(UNDERLINED), |result| {
                        context.borrow_mut().underlined = true;
                        result
                    }),
                ),
                ctx("underlined content", many1(text_component(context))),
                ctx("underlined end", cut(complete::char(UNDERLINED))),
            ),
        )(input)?;

        context.borrow_mut().underlined = false;
        Ok((remainder, TextComponent::Underlined(underlined)))
    }
}

fn text_math(input: &str) -> ParseResult<TextComponent> {
    let (remainder, math) = ctx(
        "math",
        delimited(
            ctx("math start", complete::char(MATH)),
            ctx("math content", context_str(&MarkupContext::InlineMath)),
            ctx("math end", cut(complete::char(MATH))),
        ),
    )(input)?;
    Ok((remainder, TextComponent::Math(math)))
}

fn text_code(input: &str) -> ParseResult<TextComponent> {
    let (remainder, code) = ctx(
        "code",
        delimited(
            ctx("code start", complete::char(CODE)),
            ctx("code content", context_str(&MarkupContext::InlineCode)),
            ctx("code end", cut(complete::char(CODE))),
        ),
    )(input)?;
    Ok((remainder, TextComponent::Code(code)))
}

fn context_str<'i: 'c, 'c>(
    context: &'c MarkupContext,
) -> impl Fn(&'i str) -> ParseResult<String> + 'c {
    move |input: &str| {
        fold_many1(
            alt((
                context_char_unescaped(context),
                context_char_escaped(context),
            )),
            String::new(),
            |mut acc: String, item| {
                acc.push(item);
                acc
            },
        )(input)
    }
}

fn context_char_unescaped<'i: 'c, 'c>(
    context: &'c MarkupContext,
) -> impl Fn(&'i str) -> ParseResult<char> + 'c {
    move |input: &str| none_of(context.excluded())(input)
}

fn context_char_escaped<'i: 'c, 'c>(
    context: &'c MarkupContext,
) -> impl Fn(&'i str) -> ParseResult<char> + 'c {
    move |input: &str| {
        ctx(
            "escaped char",
            preceded(
                ctx("escape", complete::char('\\')),
                ctx("char", cut(one_of(context.excluded()))),
            ),
        )(input)
    }
}

#[derive(Debug)]
struct TextComponentContext {
    bold: bool,
    italic: bool,
    strikethrough: bool,
    underlined: bool,
}

impl TextComponentContext {
    fn new() -> Self {
        TextComponentContext {
            bold: false,
            italic: false,
            strikethrough: false,
            underlined: false,
        }
    }
}

enum MarkupContext {
    Plain,
    InlineMath,
    InlineCode,
    Comment,
    CommandName,
    Parameter,
}

const PLAIN_EXCLUDED: &str = "*~_$`/\\ \t\r\n";
const MATH_EXCLUDED: &str = "$\\\r\n";
const CODE_EXCLUDED: &str = "`\\\r\n";
const COMMENT_EXCLUDED: &str = "\r\n";
const COMMAND_NAME_EXCLUDED: &str = "\\( \t\r\n";
const PARAMETER_EXCLUDED: &str = "\\,=) \t\r\n";

impl MarkupContext {
    fn excluded(&self) -> &'static str {
        match self {
            MarkupContext::Plain => PLAIN_EXCLUDED,
            MarkupContext::InlineMath => MATH_EXCLUDED,
            MarkupContext::InlineCode => CODE_EXCLUDED,
            MarkupContext::Comment => COMMENT_EXCLUDED,
            MarkupContext::CommandName => COMMAND_NAME_EXCLUDED,
            MarkupContext::Parameter => PARAMETER_EXCLUDED,
        }
    }
}
