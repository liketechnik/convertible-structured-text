// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use thiserror::Error;

// The implementation and functionality of `VerboseError` is adopted
// from nom(https://github.com/geal/nom)'s implementation of a verbose error type.
// See https://github.com/Geal/nom/blob/5.1.2/src/error.rs for the original implementation.

#[derive(Error, Debug)]
pub struct VerboseError<'i> {
    errors: Vec<(&'i str, VerboseErrorKind)>,
}

impl<'i> std::fmt::Display for VerboseError<'i> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug)]
pub enum VerboseErrorKind {
    Context(&'static str),
    Char(char),
    Nom(nom::error::ErrorKind),
    Error(anyhow::Error),
    NestedError(anyhow::Error),
}

impl<'i> nom::error::ParseError<&'i str> for VerboseError<'i> {
    fn from_error_kind(input: &'i str, kind: nom::error::ErrorKind) -> Self {
        VerboseError {
            errors: vec![(input, VerboseErrorKind::Nom(kind))],
        }
    }

    fn append(input: &'i str, kind: nom::error::ErrorKind, mut other: Self) -> Self {
        other.errors.push((input, VerboseErrorKind::Nom(kind)));
        other
    }

    fn from_char(input: &'i str, c: char) -> Self {
        VerboseError {
            errors: vec![(input, VerboseErrorKind::Char(c))],
        }
    }

    fn add_context(input: &'i str, ctx: &'static str, mut other: Self) -> Self {
        other.errors.push((input, VerboseErrorKind::Context(ctx)));
        other
    }
}

impl<'i> VerboseError<'i> {
    pub fn from_error(input: &'i str, e: anyhow::Error) -> Self {
        VerboseError {
            errors: vec![(input, VerboseErrorKind::Error(e))],
        }
    }

    pub fn from_nested(input: &'i str, e: anyhow::Error) -> Self {
        VerboseError {
            errors: vec![(input, VerboseErrorKind::NestedError(e))],
        }
    }
}

pub fn convert_error(input: &str, e: VerboseError) -> String {
    use nom::Offset;
    use std::fmt::Write;

    let mut result = String::new();

    for (i, (substring, kind)) in e.errors.iter().enumerate() {
        if input.is_empty() {
            match kind {
                VerboseErrorKind::Char(c) => {
                    write!(&mut result, "{}: expected '{}', got empty input\n\n", i, c)
                }
                VerboseErrorKind::Context(s) => {
                    write!(&mut result, "{}: in {}, got empty input\n\n", i, s)
                }
                VerboseErrorKind::Nom(e) => {
                    write!(&mut result, "{}: {:?}, got empty input\n\n", i, e)
                }
                VerboseErrorKind::Error(e) => {
                    write!(&mut result, "{}: {:?}, got empty input\n\n", i, e)
                }
                VerboseErrorKind::NestedError(e) => {
                    write!(&mut result, "{}: {:?}, got empty input\n\n", i, e)
                }
            }
        } else {
            let offset = input.offset(substring);

            let prefix = &input.as_bytes()[..offset];

            // Count the number of newlines in the first `offset` bytes of input
            #[allow(clippy::naive_bytecount)]
            let line_number = prefix.iter().filter(|&&b| b == b'\n').count() + 1;

            // Find the line that includes the subslice:
            // Find the *last* newline before the substring starts
            let line_begin = prefix
                .iter()
                .rev()
                .position(|&b| b == b'\n')
                .map(|pos| offset - pos)
                .unwrap_or(0);

            // Find the full line after that newline
            let line = input[line_begin..]
                .lines()
                .next()
                .unwrap_or(&input[line_begin..])
                .trim_end();

            // The (1-indexed) column number is the offset of our substring into that line
            let column_number = line.offset(substring) + 1;

            match kind {
                VerboseErrorKind::Char(c) => {
                    if let Some(actual) = substring.chars().next() {
                        write!(
                            &mut result,
                            "{i}: at line {line_number}:\n\
                        {line}\n\
                           {caret:>column$}\n\
                           expected '{expected}', found '{actual}'\n\n",
                            i = i,
                            line_number = line_number,
                            line = line,
                            caret = '^',
                            column = column_number,
                            expected = c,
                            actual = actual
                        )
                    } else {
                        write!(
                            &mut result,
                            "{i}: at line {line_number}:\n\
                           {line}\n\
                           {caret:>column$}\n\
                           expected '{expected}', got end of input\n\n'",
                            i = i,
                            line_number = line_number,
                            line = line,
                            caret = '^',
                            column = column_number,
                            expected = c,
                        )
                    }
                }
                VerboseErrorKind::Context(s) => write!(
                    &mut result,
                    "{i}: at line {line_number}, in {context}:\n\
                       {line}\n\
                       {caret:>column$}\n\n",
                    i = i,
                    line_number = line_number,
                    context = s,
                    line = line,
                    caret = '^',
                    column = column_number,
                ),
                VerboseErrorKind::Nom(e) => write!(
                    &mut result,
                    "{i}: at line {line_number}, in {nom_err:?}:\n\
                       {line}\n\
                       {caret:>column$}\n\n",
                    i = i,
                    line_number = line_number,
                    nom_err = e,
                    line = line,
                    caret = '^',
                    column = column_number,
                ),
                VerboseErrorKind::Error(e) => write!(
                    &mut result,
                    "{i}: at line {line_number}, {error:?}:\n\n\
                       {line}\n\
                       {caret:>column$}\n\n",
                    i = i,
                    line_number = line_number,
                    error = e,
                    line = line,
                    caret = '^',
                    column = column_number,
                ),
                VerboseErrorKind::NestedError(e) => write!(
                    &mut result,
                    "{i}: at line {line_number}:\n\
                       {line}\n\
                       {caret:>column$}\n\
                       {error:?}",
                    i = i,
                    line_number = line_number,
                    error = e,
                    line = line,
                    caret = '^',
                    column = column_number,
                ),
            }
        }
        .unwrap()
    }

    result
}
