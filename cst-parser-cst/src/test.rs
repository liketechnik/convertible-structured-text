// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use cst_api::ast::{Command, CommandContent, Document, Parameter, TextComponent};
use cst_api::config::CommandRegistry;
use cst_api::config::Configuration;
use std::collections::HashMap;

use super::MarkupContext;

fn default_runtime_config() -> Configuration {
    use clap::Clap;
    use cst_api::config::{Options, Settings};

    let opts = Options::parse_from(vec!["mockup", "-p", "test"]);
    let settings = Settings::new(&opts).unwrap();

    let command_registry = CommandRegistry {
        parser_commands: HashMap::new(),
        parser_inline_commands: HashMap::new(),
        render_commands: Vec::new(),
    };
    Configuration::new(
        settings,
        opts,
        command_registry,
        HashMap::new(),
        HashMap::new(),
    )
}

#[test]
fn text_whitespace() {
    let input = " \taklsdfjlkasdjf";
    let (_, result) = super::text_whitespace(input).unwrap();
    let expected = TextComponent::Whitespace(" \t".to_string());
    assert_eq!(result, expected);
}

#[test]
fn title_level() {
    let input = "###### An interesting title";
    let (_, result) = super::title_level(input).unwrap();
    let expected = 6;
    assert_eq!(result, expected);
}

#[test]
fn context_str_plain() {
    let input = "abcdefghiklmnop\\*";
    let (_, result) = super::context_str(&MarkupContext::Plain)(input).unwrap();
    let expected = "abcdefghiklmnop*";
    assert_eq!(result, expected);
}

#[test]
fn context_str_math() {
    let input = "123 abc`\\$";
    let (_, result) = super::context_str(&MarkupContext::InlineMath)(input).unwrap();
    let expected = "123 abc`$";
    assert_eq!(result, expected);
}

#[test]
fn context_str_code() {
    let input = "123\tabc_$/*~\\`\\\\";
    let (_, result) = super::context_str(&MarkupContext::InlineCode)(input).unwrap();
    let expected = "123\tabc_$/*~`\\";
    assert_eq!(result, expected);
}

#[test]
fn title() {
    let input = "### abc **test**";
    let (_, result) = super::title(&default_runtime_config())(input).unwrap();
    let expected = Document::Title {
        level: 3,
        text: vec![
            TextComponent::Plain("abc".to_string()),
            TextComponent::Whitespace(" ".to_string()),
            TextComponent::Italic(vec![TextComponent::Plain("test".to_string())]),
        ],
    };
    assert_eq!(result, expected);
}

#[test]
fn comment() {
    let input = "// dlksajfljasdlfjl \\ dslkfjnbbdasfyqkion\n";
    let (_, result) = super::comment(input).unwrap();
    let expected = Document::Comment(" dlksajfljasdlfjl \\ dslkfjnbbdasfyqkion".to_string());
    assert_eq!(result, expected);
}

#[test]
fn paragraph() {
    let input = "abcdefg and what about words   with multiple whitespaces\n*and bold?* 12334656\n\n# abcdefg";
    let (_, result) = super::paragraph(&default_runtime_config())(input).unwrap();
    let expected = Document::Paragraph(vec![
        TextComponent::Plain("abcdefg".to_string()),
        TextComponent::Whitespace(" ".to_string()),
        TextComponent::Plain("and".to_string()),
        TextComponent::Whitespace(" ".to_string()),
        TextComponent::Plain("what".to_string()),
        TextComponent::Whitespace(" ".to_string()),
        TextComponent::Plain("about".to_string()),
        TextComponent::Whitespace(" ".to_string()),
        TextComponent::Plain("words".to_string()),
        TextComponent::Whitespace("   ".to_string()),
        TextComponent::Plain("with".to_string()),
        TextComponent::Whitespace(" ".to_string()),
        TextComponent::Plain("multiple".to_string()),
        TextComponent::Whitespace(" ".to_string()),
        TextComponent::Plain("whitespaces".to_string()),
        TextComponent::Whitespace("".to_string()),
        TextComponent::Bold(vec![
            TextComponent::Plain("and".to_string()),
            TextComponent::Whitespace(" ".to_string()),
            TextComponent::Plain("bold?".to_string()),
        ]),
        TextComponent::Whitespace(" ".to_string()),
        TextComponent::Plain("12334656".to_string()),
    ]);
    assert_eq!(result, expected);
}

#[test]
fn command() {
    let input = "/include(param1=some\\ val,    param2=someother,param3=something,     path=\\\\~/$te\\,st_\\ .png){ cooler\n content /}";
    let (_, result) = super::command(input).unwrap();
    let expected = Command {
        name: "include".to_string(),
        parameters: vec![
            Parameter {
                name: "param1".to_string(),
                value: "some val".to_string(),
            },
            Parameter {
                name: "param2".to_string(),
                value: "someother".to_string(),
            },
            Parameter {
                name: "param3".to_string(),
                value: "something".to_string(),
            },
            Parameter {
                name: "path".to_string(),
                value: "\\~/$te,st_ .png".to_string(),
            },
        ],
        content: CommandContent::Raw(" cooler\n content ".to_string()),
    };
    assert_eq!(result, expected);
}

#[test]
fn bold() {
    let input = "~asdf~fasdf adf";
    let (_, _result) = super::text_line(&default_runtime_config())(input).unwrap();
}
