// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use docx_rs::{Style, StyleType, Styles};

pub fn styles() -> Styles {
    Styles::new()
        .add_style(heading_style())
        .add_style(heading_style_2())
        .add_style(normal_style())
}

pub fn heading_style() -> Style {
    Style::new("heading1", StyleType::Paragraph)
        .size(30)
        .name("Heading Level 1")
}

pub fn heading_style_2() -> Style {
    Style::new("heading2", StyleType::Paragraph)
        .size(20)
        .name("Heading Level 2")
}

pub fn normal_style() -> Style {
    Style::new("normal", StyleType::Paragraph).name("Normal Text from Export")
}
