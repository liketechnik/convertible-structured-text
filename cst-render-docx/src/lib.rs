// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use anyhow::Result;
use cst_api::ast::Document;
use cst_api::config::Configuration;
use docx_rs::Docx;
use std::path::Path;

pub mod render;
pub mod styles;

fn render(nodes: &[Document], out_file: &Path, config: &Configuration) -> Result<()> {
    let mut document = Docx::new().styles(styles::styles());

    for paragraph in render::render_ast(nodes, config) {
        document = document.add_paragraph(paragraph);
    }

    document.build().pack(std::fs::File::create(out_file)?)?;
    Ok(())
}

cst_api::export_render!("docx", render, &["image"]);
