// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use cst_api::ast::{Command, CommandContent, Document, TextComponent};
use cst_api::config::Configuration;
use docx_rs::Paragraph;
use docx_rs::Pic;
use docx_rs::Run;

pub fn render_ast(nodes: &[Document], config: &Configuration) -> Vec<Paragraph> {
    let mut paragraphs = vec![];

    for node in nodes {
        match node {
            Document::Comment(_) => (),
            Document::Command(command) => match command.name.as_str() {
                "image" => {
                    let mut paragraph = Paragraph::new();
                    paragraph = paragraph.style("normal");

                    let mut run = Run::new();
                    run = render_image(run, command, config);

                    paragraph = paragraph.add_run(run);

                    paragraphs.push(paragraph);
                }
                _ => panic!("Unsupported command: {}", command.name),
            },

            Document::Paragraph(content) => {
                let paragraph = Paragraph::new();
                let mut paragraph = paragraph.style("normal");

                for run in render_text_components(&content, config) {
                    paragraph = paragraph.add_run(run);
                }

                paragraphs.push(paragraph);
            }
            Document::Title { level: _, text } => {
                let paragraph = Paragraph::new();
                let mut paragraph = paragraph.style("heading1");

                for run in render_text_components(&text, config) {
                    paragraph = paragraph.add_run(run);
                }

                paragraphs.push(paragraph);
            }
        }
    }

    paragraphs
}

fn render_text_components(nodes: &[TextComponent], config: &Configuration) -> Vec<Run> {
    let mut runs = vec![];

    let mut previous_whitespace = false;

    for node in nodes {
        if let TextComponent::Whitespace(_) = node {
            if previous_whitespace {
                continue;
            }
            previous_whitespace = true;
        } else {
            previous_whitespace = false;
        }

        let mut run = Run::new();
        run = render_text_component(run, node, config);
        runs.push(run);
    }

    runs
}

fn render_text_component(mut run: Run, node: &TextComponent, config: &Configuration) -> Run {
    match node {
        TextComponent::Whitespace(_) => {
            run = run.add_text(" ");
        }
        TextComponent::Italic(content) => {
            run = run.italic();

            for node in content {
                run = render_text_component(run, node, config);
            }
        }
        TextComponent::Bold(content) => {
            run = run.bold();

            for node in content {
                run = render_text_component(run, node, config);
            }
        }
        TextComponent::Strikethrough(content) => {
            run = run.color("red");
            for node in content {
                run = render_text_component(run, node, config);
            }
        }
        TextComponent::Underlined(content) => {
            run = run.underline("Normal");

            for node in content {
                run = render_text_component(run, node, config);
            }
        }
        TextComponent::Math(content) => {
            run = run.color("yellow");
            run = run.add_text(content);
        }
        TextComponent::Code(content) => {
            run = run.color("blue");
            run = run.add_text(content);
        }
        TextComponent::Plain(content) => {
            run = run.add_text(content);
        }
        TextComponent::Command(command) => match command.name.as_str() {
            "image" => {
                run = render_image(run, command, config);
            }
            _ => panic!("Unsupported command: {}", command.name),
        },
    };

    run
}

fn render_image(mut run: Run, command: &Command, config: &Configuration) -> Run {
    let content = match &command.content {
        CommandContent::Raw(content) => content,
        _ => panic!("Invalid command content"),
    };

    let mut image_path = config.build.in_dir.clone();
    image_path.push(content);
    let image_data = std::fs::read(image_path).unwrap();
    let image = Pic::new(image_data);

    run = run.add_image(image);

    run
}
